name := "market-logic-booking"

version := "0.1"

scalaVersion := "2.12.4"

logLevel := Level.Warn

libraryDependencies ++= Seq(
  "com.github.nscala-time" %% "nscala-time" % "2.16.0",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test"
)