The Booking system home task for Market Logic.

How to run:

1. Install SBT `apt-get install sbt` or `brew install sbt`
2. Run local tests `sbt test`
3. Run with data `sbt run < <your-file>`. Calendar should be printed to STDOUT with some SBT debug output.
