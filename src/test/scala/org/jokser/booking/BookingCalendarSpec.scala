package org.jokser.booking

import com.github.nscala_time.time.Imports._
import org.jokser.booking.BookingCalendar.roundUpToDays
import org.scalatest.{FlatSpec, Inspectors, Matchers}

import scala.util.Success

class BookingCalendarSpec extends FlatSpec with Matchers with Inspectors {

  private val submissionDate = roundUpToDays(DateTime.now())
  private val requestDate = submissionDate + 1.day
  private val dayStartTime = new LocalTime(9, 0)
  private val dayEndTime = new LocalTime(18, 0)

  it should "save booking on available date" in {
    val bookingCalendar = new BookingCalendar(dayStartTime, dayEndTime)
    val request = BookingRequest("id", submissionDate, requestDate.withHour(10).withMinute(0), 5)
    val booking = request.toBooking

    bookingCalendar.book(request) shouldBe Success((requestDate, booking))

    bookingCalendar.calendar should contain (requestDate -> List(booking))
  }

  it should "save non-overlap bookings on same day" in {
    val bookingCalendar = new BookingCalendar(dayStartTime, dayEndTime)
    val requests = List(
      BookingRequest("id", submissionDate, requestDate.withHour(10).withMinute(0), 2),
      BookingRequest("id", submissionDate, requestDate.withHour(17).withMinute(0), 1),
      BookingRequest("id", submissionDate, requestDate.withHour(13).withMinute(0), 1)
    )

    forAll(requests) { request =>
      bookingCalendar.book(request) shouldBe Success((roundUpToDays(request.requestedDate), request.toBooking))
    }

    val expectedBookings = requests.map(_.toBooking)

    bookingCalendar.calendar.get(requestDate).head should contain only (expectedBookings: _*)
  }

  it should "save non-overlap bookings on several days" in {
    val bookingCalendar = new BookingCalendar(dayStartTime, dayEndTime)
    val requests = List(
      BookingRequest("id", submissionDate, requestDate.withHour(10).withMinute(0), 2),
      BookingRequest("id", submissionDate, requestDate.withHour(17).withMinute(0) + 1.day, 1),
      BookingRequest("id", submissionDate, requestDate.withHour(13).withMinute(0) + 2.day, 1)
    )

    forAll(requests) { request =>
      bookingCalendar.book(request) shouldBe Success((roundUpToDays(request.requestedDate), request.toBooking))
    }

    bookingCalendar.calendar should have size 3
  }

  it should "reject booking if it's not within available times" in {
    val bookingCalendar = new BookingCalendar(dayStartTime, dayEndTime)

    an [IllegalArgumentException] should be thrownBy {
      bookingCalendar.book(
        BookingRequest("id", submissionDate, requestDate.withHour(8).withMinute(0), 2)
      ).get
    }

    an [IllegalArgumentException] should be thrownBy {
      bookingCalendar.book(
        BookingRequest("id", submissionDate, requestDate.withHour(17).withMinute(0), 2)
      ).get
    }

    bookingCalendar.calendar should have size 0
  }

  it should "reject booking if it overlaps with existing" in {
    val bookingCalendar = new BookingCalendar(dayStartTime, dayEndTime)

    val request = BookingRequest("id", submissionDate, requestDate.withHour(10).withMinute(0), 2)
    val existingBooking = bookingCalendar.book(request).get._2

    the [IllegalArgumentException] thrownBy {
      bookingCalendar.book(
        BookingRequest("id", submissionDate, requestDate.withHour(11).withMinute(30), 1)
      ).get
    } should have message ("Requested booking overlaps with " + existingBooking)

    bookingCalendar.calendar should contain only (requestDate -> List(existingBooking))
  }
}
