package org.jokser.booking

import java.io.BufferedReader

import com.github.nscala_time.time.Imports._

import scala.util.Try

class BookingsParser(reader: BufferedReader) {

  val availableTimeFormat = "HHmm"
  val submissionFormat = "YYYY-MM-dd HH:mm:ss"
  val requestFormat = "YYYY-MM-dd HH:mm"

  def parseAvailableTimes(): (LocalTime, LocalTime) = {
    val times = reader.readLine().split(" ")
    times match {
      case Array(startTime, endTime) =>
        (startTime.toDateTime(availableTimeFormat).toLocalTime,
          endTime.toDateTime(availableTimeFormat).toLocalTime)
      case _ =>
        throw new IllegalArgumentException("Unexpected format of available times " + times)
    }
  }

  def nextBookingRequest(): Try[BookingRequest] = Try {
    val submissionLine = reader.readLine()

    val (submissionDate, employeeId) = submissionLine.split(" ") match {
      case Array(date, time, id) =>
        ((date + " " + time).toDateTime(submissionFormat), id)
      case _ =>
        throw new IllegalArgumentException("Unexpected format of submission line " + submissionLine)
    }

    val requestLine = reader.readLine()

    val (requestedDate, requestedHours) = requestLine.split(" ") match {
      case Array(date, time, hours) =>
        ((date + " " + time).toDateTime(requestFormat), hours.toInt)
      case _ =>
        throw new IllegalArgumentException("Unexpected format of request line " + requestLine)
    }

    BookingRequest(employeeId, submissionDate, requestedDate, requestedHours)
  }
}
