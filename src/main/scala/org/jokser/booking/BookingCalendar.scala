package org.jokser.booking

import com.github.nscala_time.time.Imports._
import org.jokser.booking.BookingCalendar.roundUpToDays

import scala.collection.mutable
import scala.util.Try

class BookingCalendar(dayStartTime: LocalTime,
                      dayEndTime: LocalTime) {

  val calendar = new mutable.TreeMap[DateTime, List[Booking]]()

  def book(request: BookingRequest): Try[(DateTime, Booking)] = Try {
    val requestedBooking = request.toBooking

    require(
      requestedBooking.startTime >= dayStartTime && requestedBooking.endTime <= dayEndTime,
      s"Requested time must be within available times $dayStartTime - $dayEndTime"
    )

    val requestedDate = roundUpToDays(request.requestedDate)

    val existingBookings = calendar.get(requestedDate)

    existingBookings match {
      case None =>
        calendar.put(requestedDate, List(requestedBooking))
      case Some(bookings) =>
        val overlap = bookings.find(existing => existing.overlapsWith(requestedBooking))
        overlap match {
          case None =>
            calendar.put(requestedDate, requestedBooking :: bookings)
          case Some(booking) =>
            throw new IllegalArgumentException("Requested booking overlaps with " + booking)
        }
    }

    (requestedDate, requestedBooking)
  }

  override def toString: String = {
    calendar.map { case (date: DateTime, bookings: List[Booking]) =>
      s"""${date.toString("YYYY-MM-dd")}
         |${bookings.sorted.mkString("\n")}""".stripMargin
    }.mkString("\n")
  }
}

object BookingCalendar {
  def roundUpToDays(date: DateTime): DateTime = date.dayOfMonth.roundDown
}

case class Booking(employeeId: String,
                   startTime: LocalTime,
                   endTime : LocalTime) extends Ordered[Booking] {

  def overlapsWith(other: Booking): Boolean = {
    if (startTime >= other.endTime)
      return false
    if (other.startTime >= endTime)
      return false

    true
  }

  override def toString =
    s"${startTime.toString("HH:mm")} ${endTime.toString("HH:mm")} $employeeId"

  override def compare(that: Booking): Int = startTime.compareTo(that.startTime)
}

case class BookingRequest(employeeId: String,
                          submissionDate: DateTime,
                          requestedDate: DateTime,
                          requestedHours: Int) extends Ordered[BookingRequest] {
  //TODO: Case to check - If requested hours is high number, end time of booking can be on the next day after requested.
  require(requestedDate >= submissionDate, "Requested date must be greater than submission date")

  def toBooking: Booking = {
    val startTime = requestedDate.toLocalTime
    Booking(employeeId, startTime, startTime + requestedHours.hours)
  }

  override def compare(that: BookingRequest): Int = submissionDate.compare(that.submissionDate)
}
