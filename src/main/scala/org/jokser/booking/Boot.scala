package org.jokser.booking

import java.io.{BufferedReader, InputStreamReader}

object Boot extends App {
  val parser = new BookingsParser(new BufferedReader(new InputStreamReader(System.in)))

  val (startTime, endTime) = parser.parseAvailableTimes()
  val calendar = new BookingCalendar(startTime, endTime)

  // Read requests while possible, sort and process them
  Stream.continually(parser.nextBookingRequest())
    .takeWhile(_.isSuccess)
    .map(request => request.get)
    .sorted
    .foreach(request => calendar.book(request))

  println(calendar)
}
